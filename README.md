# coq-mods-jellyface
*The Jellyface mod adds the new Jellyface creature to Caves of Qud.*

<p>
  <img src="/images/screen4.png" width="18%" />
  <img src="/images/screen.png" width="16%" />
  <img src="/images/screen1.png" width="18%" /> 
  <img src="/images/screen3.png" width="18%" />
  <img src="/images/screen5.png" width="18%" />
</p>

## ![](/Textures/jellyface.png)  Description
This is a mod for [Caves of Qud](https://freeholdgames.itch.io/cavesofqud) by [Freehold Games](https://www.freeholdgames.com/), which was made for the [Monster Mash modding jam](https://itch.io/jam/caves-of-qud-modding-jam-1).

For further details check out our [itch.io submission page](https://k0ba-n-miliv.itch.io/coq-jellyface)!

## ![](/Textures/jellyface.png)  Who are Jellyface?
*Jellyface are remnants of a bygone era where bio-engineering was at its peak. Created by the Eaters as a hybrid between human cognition and jellyfish physiology, they were designed to serve as caretakers for the deep aquatic laboratories of Qud, where Jellyface have been an integral part of the Eaters` society.*

*They have been specifically designed to facilitate communication among the Eaters and to embody the ethos of the Eaters, serving as living examples of their advanced understanding of life and their pursuit of harmony with Qud’s diverse ecosystems.*

*Their psychic energies have been harnessed to create a network of telepathic links, allowing the Eaters to share thoughts and wisdom instantaneously across great distances.*

*When the cataclysm shattered the old world, Jellyface and their kind were freed from their servitude.*

*Driven by fragmented memories of their creation and remnants of human-like consciousness, Jellyface are on a quest to uncover the truth of their origins and the strips of the civilization that conceived them. As the Eaters' creations, they carry ancient memories and genetic codes that could unlock secrets of the Eaters' technologies or philosophies.*

*A peaceful, yet misunderstood species, they now roam the waterlogged passages of Qud, their origin forgotten by most.*

## ![](/Textures/jellyface.png)  Installation
For installing the mod follow this amazing and descriptive [guide](https://wiki.cavesofqud.com/wiki/Modding:Tutorial_-_Snapjaw_Mages).

## ![](/Textures/jellyface.png)  Credits
This is our first game jam ever together as: 
- @milenaveleva / xml, pixel art, itch design, ideation
- @k0ba / conversation design, lore, testing, ideation

## ![](/Textures/jellyface.png)  License
Creative Commons Zero.

## ![](/Textures/jellyface.png)  Project status
Prototype.
